package com.danikula.backstack;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * @author Alexey Danilov (danikula@gmail.com).
 */
public class AlphabetFragment extends Fragment {

    private static final String ARG_TEXT = "text";

    public static Fragment newInstance(String text) {
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, text);
        Fragment fragment = new AlphabetFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle state) {
        View content = inflater.inflate(R.layout.fragment_alphabet, null);
        TextView textView = (TextView) content;
        String text = getArguments().getString(ARG_TEXT, "WTF?");
        textView.setText(text);
        return content;
    }
}
