package com.danikula.backstack;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    private static final String[] NAMES;

    static {
        NAMES = new String['z'-'a'];
        for (char i = 'a'; i < 'z'; i++) {
            NAMES[i-'a'] = Character.toString(i);
        }
    }

    private int fragmentIndex;
    private Button pushButton;
    private EditText nameEditText;
    private TextView countTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        pushButton = (Button) findViewById(R.id.pushButton);
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        countTextView = (TextView) findViewById(R.id.countTextView);
        updateUi();
    }

    public void pushFragment(View view) {
        Fragment fragment = AlphabetFragment.newInstance(getName());
        pushFragment(getName(), fragment);
        fragmentIndex++;
        updateUi();
    }

    private void updateUi() {
        pushButton.setText("Push " + getName());
    }

    private String getName() {
        return NAMES[fragmentIndex];
    }

    private void pushFragment(String name, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment, name)
                .addToBackStack(name)
                .commit();
    }

    public void popFragment(View view) {
        String name = nameEditText.getText().toString();
        if (TextUtils.isEmpty(name)) {
            getSupportFragmentManager().popBackStack();
        } else {
            getSupportFragmentManager().popBackStack(name, 0);
        }

        updateUi();
    }

    @Override
    public void onBackStackChanged() {
        countTextView.setText("Back stack size is " + getSupportFragmentManager().getBackStackEntryCount());
    }
}
